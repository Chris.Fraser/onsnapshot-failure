module.exports = {
  apps: [
    {
      name: 'onsnapshot-failure',
      script: './dist/index.js',
      instances: 1,
      autorestart: true
    }
  ],
  deploy: {
    production: {
      user: 'deploy',
      key: '~/.ssh/deploy_user_id_rsa',
      host: ['az-za-02.italkdevice.com'],
      ref: 'master',
      repo: 'git@gitlab.com:Chris.Fraser/onsnapshot-failure.git',
      path: '/home/deploy/pm2/onsnapshot-failure',
      'post-setup': 'mkdir -p ~/failedRecordings && mkdir -p ~/logs',
      'post-deploy': 'npm install && npm install --only=dev && npm run build-ts && pm2 startOrRestart ecosystem.config.js'
    }
  }
};
